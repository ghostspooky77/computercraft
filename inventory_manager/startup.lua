local im = peripheral.wrap("left")
local modem = peripheral.wrap("back")

local im_channel = 9100
modem.open(im_channel)

local enabled = false

whitelisted_items = {
    "mekanism:meka_tool",
    "draconicevolution:chaotic_bow",
    "universalgrid:creative_wireless_universal_grid",
    "draconicevolution:chaotic_capacitor",
    "sophisticatedbackpacks:netherite_backpack",
    "mahoutsukai:morgan",
    "xreliquary:angelheart_vial",
    "tiab:timeinabottle",
    "archers_paradox:diamond_arrow",
    "forbidden_arcanus:spectral_eye_amulet",
    "silentgear:pickaxe",
    "computercraft:pocket_computer_advanced",
    "naturescompass:naturescompass",
    "draconicevolution:advanced_magnet",
    "hostilenetworks:deep_learner"
}

function is_whitelisted(item)
    for i, v in ipairs(whitelisted_items) do
        if v == item then
            return true
        end
    end
    return false
end

function remove_not_whitelisted_items()
    local inventory_items = im.getItems()
    for i, v in pairs(inventory_items) do
        if(not is_whitelisted(inventory_items[i].name) and enabled) then
            im.removeItemFromPlayer("UP", 64, -1, inventory_items[i].name)
        end
    end
end

function get_player_whitelisted_items()
    local inventory_items = im.getItems()
    for i, v in pairs(inventory_items) do
        if(is_whitelisted(inventory_items[i].name)) then
            print(inventory_items[i].name .. " is whitelisted.")
        else
            print(inventory_items[i].name .. " is not whitelisted.")
        end
    end
end

function listener()
  repeat
    event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
  until channel == im_channel
  print("Received request \"" .. message .. "\" from " .. replyChannel)
  if(message == "im enable") then
    enabled = true
    modem.transmit(replyChannel, im_channel, "Inventory Manager enabled.")
  elseif(message == "im disable") then
    enabled = false
    modem.transmit(replyChannel, im_channel, "Inventory Manager disabled.")
  else
    modem.transmit(replyChannel, im_channel, "Received unhandled message " .. message)
  end
end

while(true) do
    parallel.waitForAny(listener, remove_not_whitelisted_items)
    --remove_not_whitelisted_items()
end

