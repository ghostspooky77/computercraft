local reactor = peripheral.wrap("back")
local monitor = peripheral.wrap("left")
local modem = peripheral.wrap("right")

local matrix_channel = 9000
local reactor_channel = 9001
local tank_channel = 9002
local cc_channel = 9003
local sps_channel = 9004
local tube_channel = 9005

local reactor_switch = false

modem.open(reactor_channel)

function comma_value(amount)
  local formatted = amount
  while true do
    formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
    if (k==0) then
      break
    end
  end
  return formatted
end

function round(val, decimal)
  if (decimal) then
    return math.floor((val*10^decimal)+0.5)/(10^decimal)
  else
    return math.floor(val+0.5)
  end
end 

function requestData(request_channel, request)
  local event, side, channel, replyChannel, message, distance
  print("Requesting " .. request .. " from " .. request_channel)
  modem.transmit(request_channel, reactor_channel, request)
  repeat
    event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
  until channel == reactor_channel
  print("Received " .. request .. ":" .. message)
  return message
end

function monitorWriteLine(input_string, color, line)
  monitor.setCursorPos(1,line)
  monitor.clearLine()
  monitor.setTextColor(color)
  monitor.write(input_string, color)
end
  
function displayReactorFuel()
  local stored_fuel = requestData(tank_channel, "get_tank_stored_amount")
  local reactor_fuel = 0
  if reactor ~= nil then
   reactor_fuel = reactor.getFuel().amount
  end
  monitorWriteLine(comma_value(reactor_fuel), colors.white, 2)
  monitorWriteLine(comma_value(stored_fuel + reactor_fuel), colors.white, 4)
end

function displayStoredEnergy()
  local conv_rate = 1000000000 * 2.5
  local energy = round(requestData(matrix_channel, "get_stored_energy") / conv_rate, 2)
  local energy_input = requestData(matrix_channel, "get_energy_input")
  local energy_output = requestData(matrix_channel, "get_energy_output")
  local energy_profit = round((energy_input - energy_output) / conv_rate * 1000000, 2)
  local energy_filled_percent = round(requestData(matrix_channel, "get_energy_filled_percent"))
  safetyCheck(energy_filled_percent)
  monitorWriteLine(energy .. " GFE (" .. energy_filled_percent .. "%)", colors.white, 6)
  monitorWriteLine("+" .. energy_profit .. "k FE/t", colors.white, 7)
end

function displayPolonium()
  local sps_polonium = requestData(sps_channel, "get_machine_input_amount")
  local sps_tube_buffer = requestData(tube_channel, "get_tube_buffer_amount")
  monitorWriteLine(tostring(sps_polonium + sps_tube_buffer) .. " mB", colors.white, 9)
end

function displayAntimatter()
  local antimatter = requestData(cc_channel, "get_machine_input_amount")
  monitorWriteLine(tostring(antimatter) .. " mB", colors.white, 11)
end

function safetyCheck(energy_filled_percent)
  if(energy_filled_percent >= 98 and reactor.getStatus() and not reactor_switch) then
    reactor.scram()
    reactor_switch = true
    -- TODO: redstone switch to trash ~25% energy instead
  elseif(energy_filled_percent <= 90 and not reactor.getStatus() and reactor_switch and reactor.getWaste().amount < 500 and reactor.getTemperature() < 420) then
    reactor.activate()
    reactor_switch = false
  end
end

function getTime()
  return os.epoch("utc") / 1000
end

monitor.clear()
monitor.setCursorBlink(false)
monitor.setCursorPos(1,1)

monitorWriteLine("Reactor Fuel", colors.green, 1)
monitorWriteLine("Total Fuel", colors.green, 3)
monitorWriteLine("Energy", colors.yellow, 5)
monitorWriteLine("Polonium", colors.cyan, 8)
monitorWriteLine("Antimatter", colors.purple, 10)

while(true) do
  displayReactorFuel()
  displayStoredEnergy()
  displayPolonium()
  displayAntimatter()
end

