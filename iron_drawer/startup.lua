local drawer = peripheral.wrap("bottom")
local monitor = peripheral.wrap("top")

local iron_per_hour = 0

monitor.clear()
monitor.setCursorBlink(false)
monitor.setCursorPos(1,1)

function monitorWriteLine(input_string, color, line)
  monitor.setCursorPos(1,line)
  monitor.clearLine()
  monitor.setTextColor(color)
  monitor.write(input_string, color)
end

function count_drawer_items_per_minute()
    local count_a = drawer.getItemDetail(1).count
    --print("count_a: " .. count_a)
    wait(60)
    local count_b = drawer.getItemDetail(1).count
    --print("count_b: " .. count_b)
    if(count_b > count_a) then
        items_per_hour = (count_b - count_a) * 60
        if(iron_per_hour ~= 0) then
          iron_per_hour = (iron_per_hour + items_per_hour) / 2
        else
          iron_per_hour = items_per_hour
        end
        print(items_per_hour)
        monitorWriteLine(iron_per_hour, colors.white, 4)
    end
end

function wait(seconds)
  local time_a = os.epoch("utc")
  local time_b = 1
    while(time_b - time_a <= 1000 * seconds) do
      os.queueEvent("randomEvent")
      os.pullEvent()
      time_b = os.epoch("utc")
    end
end

monitorWriteLine("Iron", colors.white, 1)
monitorWriteLine("per", colors.white, 2)
monitorWriteLine("hour", colors.white, 3)

while(true) do
  count_drawer_items_per_minute()
end
