local modem = peripheral.wrap("right")
local tank = peripheral.wrap("back")
local qe = peripheral.wrap("quantumEntangloporter_0")

local tank_channel = 9002
modem.open(tank_channel)

local event, side, channel, replyChannel, message, distance

while(true) do
  local tank_stored_amount
  repeat
    event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
  until channel == tank_channel
  print("Received request " .. message .. " from " .. replyChannel)
  if(message == "get_tank_stored_amount") then
    tank_stored_amount = tank.getStored().amount
    qe_buffer_gas = qe.getBufferGas().amount
    modem.transmit(replyChannel, tank_channel, tank_stored_amount + qe_buffer_gas)
  end
end

