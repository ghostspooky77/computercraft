local modem = peripheral.find("modem")
local sps
while(sps == nil) do
  sps = peripheral.wrap("back")
  os.queueEvent("randomEvent")
  os.pullEvent()
end

local sps_channel = 9004
modem.open(sps_channel)

local event, side, channel, replyChannel, message, distance

while(true) do
  local machine_input_amount
  repeat
    event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
  until channel == sps_channel
  print("Received request " .. message .. " from " .. replyChannel)
  if(message == "get_machine_input_amount") then
    machine_input_amount = sps.getInput().amount
    modem.transmit(replyChannel, sps_channel, machine_input_amount)
  end
end

