certus_drawer = peripheral.wrap("functionalstorage:oak_1_2")
certus_dust_drawer = peripheral.wrap("functionalstorage:oak_1_3")
charged_certus_drawer = peripheral.wrap("functionalstorage:oak_1_4")
fluix_dust_drawer = peripheral.wrap("functionalstorage:oak_1_5")
fluix_drawer = peripheral.wrap("functionalstorage:oak_1_6")
charger1 = peripheral.wrap("extendedae:ex_charger_0")
charger2 = peripheral.wrap("extendedae:ex_charger_1")
inscriber = peripheral.wrap("extendedae:ex_inscriber_0")
chamber = peripheral.wrap("advanced_ae:reaction_chamber_1")
charged_crystal = "ae2:charged_certus_quartz_crystal"


function check_chargers(charger, slot)
	if charger.list()[slot] ~= nil then
		if charger.list()[slot].name == charged_crystal then
			charger.pushItems(peripheral.getName(charged_certus_drawer), slot)
		end
	else
		if next(certus_drawer.list()) ~= nil then
			if certus_drawer.list()[1].count > 16 then
				charger.pullItems(peripheral.getName(certus_drawer), 1, 1, slot)
			end
		end
	end
end


while(true) do
	check_chargers(charger1, 1)
	check_chargers(charger1, 2)
	check_chargers(charger1, 3)
	check_chargers(charger1, 4)
	check_chargers(charger2, 1)
	check_chargers(charger2, 2)
	check_chargers(charger2, 3)
	check_chargers(charger2, 4)
	
	if next(certus_drawer.list()) ~= nil then
		if certus_drawer.list()[1].count == 16 then
			print("Crafting 16 certus dust")
			inscriber.pullItems(peripheral.getName(certus_drawer), 1, 16, 7)
		end
	end
	
	
	if inscriber.list()[4] ~= nil then
		if inscriber.list()[4].name == "ae2:fluix_dust" and inscriber.list()[4].count == 32 then
			print("Moving 32 fluix dust from inscriber to drawer")
			inscriber.pushItems(peripheral.getName(fluix_dust_drawer), 4, 32)
		end
	end
	if inscriber.list()[8] ~= nil then
		if inscriber.list()[8].name == "ae2:certus_quartz_dust" and inscriber.list()[8].count == 16 then
			print("Moving 16 certus dust from inscriber to drawer")
			inscriber.pushItems(peripheral.getName(certus_dust_drawer), 8, 16)
		end
	end
	
	
	if next(fluix_dust_drawer.list()) ~= nil and chamber.list() ~= nil then
		if next(chamber.list()) == nil then
			if next(charged_certus_drawer.list()) ~= nil then
				if fluix_dust_drawer.list()[1].count >= 32 and charged_certus_drawer.list()[1].count >= 32 then
					print("Crafting 64 fluix crystals")
					chamber.pullItems(peripheral.getName(fluix_dust_drawer), 1, 32)
					chamber.pullItems(peripheral.getName(charged_certus_drawer), 1, 32)
				end
			end
		end
	else
		if next(inscriber.list()) == nil then
			print("Crafting 32 fluix dust")
			inscriber.pullItems(peripheral.getName(fluix_drawer), 1, 32)
		end
	end
	

	if chamber.getItemDetail(4) ~= nil then
		os.sleep(0.25)
		if chamber.list()[4].name == "ae2:fluix_crystal" then
			print("Moving 64 fluix crystals from chamber to drawer")
			chamber.pushItems(peripheral.getName(fluix_drawer), 4, 64)
		elseif chamber.list()[4].name == "ae2:certus_quartz_crystal" then
			print("Moving 64 certus crystals from chamber to drawer")
			chamber.pushItems(peripheral.getName(certus_drawer), 4, 64)
		end
	end
	
	
	if next(certus_dust_drawer.list()) ~= nil and next(charged_certus_drawer.list()) ~= nil then
		if certus_dust_drawer.list()[1].count == 16 and charged_certus_drawer.list()[1].count == 16 then
			if chamber.list()[1] == nil and chamber.list()[2] == nil then
				print("Crafting 64 certus crystals")
				chamber.pullItems(peripheral.getName(certus_dust_drawer), 1, 16)
				chamber.pullItems(peripheral.getName(charged_certus_drawer), 1, 16)
			end
		end
	end
end