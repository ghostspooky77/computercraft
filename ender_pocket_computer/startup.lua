local modem = peripheral.wrap("back")
local ender_pocket_computer_port = 9101
modem.open(ender_pocket_computer_port)

function sendMessage(destination, input)
  local event, side, channel, replyChannel, message, distance
  modem.transmit(destination, ender_pocket_computer_port, input)
  print("Sending \"" .. input .. "\" to " .. destination)
  repeat
    event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
  until channel == ender_pocket_computer_port
  print(message)
  --return message
end

while(true) do
  local input = read()
  if input == "im enable" then
      sendMessage(9100, input)
  elseif input == "im disable" then
      sendMessage(9100, input)
  else
      print("im enable/im disable")
  end
end
