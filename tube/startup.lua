local modem = peripheral.find("modem")
local tube = peripheral.wrap("back")

local tube_channel = 9005
modem.open(tube_channel)

local event, side, channel, replyChannel, message, distance

while(true) do
  repeat
    event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
  until channel == tube_channel
  print("Received request " .. message .. " from " .. replyChannel)
  if(message == "get_tube_buffer_amount") then
    tube_buffer_amount = tube.getBuffer().amount
    modem.transmit(replyChannel, tube_channel, tube_buffer_amount)
  end
end

