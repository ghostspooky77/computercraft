local modem = peripheral.find("modem")
local matrix = peripheral.wrap("back")

local matrix_channel = 9000
modem.open(matrix_channel)

local event, side, channel, replyChannel, message, distance

while(true) do
  repeat
    event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
  until channel == matrix_channel
  print("Received request " .. message .. " from " .. replyChannel)
  if(message == "get_stored_energy") then
    modem.transmit(replyChannel, matrix_channel, matrix.getEnergy())
  elseif(message == "get_energy_input") then
    modem.transmit(replyChannel, matrix_channel, matrix.getLastInput())
  elseif(message == "get_energy_output") then
    modem.transmit(replyChannel, matrix_channel, matrix.getLastOutput())
  elseif(message == "get_energy_filled_percent") then
    modem.transmit(replyChannel, matrix_channel, matrix.getEnergyFilledPercentage() * 100)
  end
end

