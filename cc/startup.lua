local modem = peripheral.find("modem")
local cc = peripheral.wrap("left")

local cc_channel = 9003
modem.open(cc_channel)

local event, side, channel, replyChannel, message, distance

while(true) do
  repeat
    event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
  until channel == cc_channel
  print("Received request " .. message .. " from " .. replyChannel)
  if(message == "get_machine_input_amount") then
    machine_input_amount = cc.getInput().amount
    modem.transmit(replyChannel, cc_channel, machine_input_amount)
  end
end

